# Amanda

Link to Original Repo: https://github.com/brandon-hiles/Amanda

Amanda is a python data mining assistant, currently still in beta (version 0.1), which mines
data from economic news sites. This bot works off the sitemap data provided openly
by the news sites

Note: Amanda is written in Python3, NOT Python 2.7.x