# erika

Erika will be an AI investment assistant, that is capable of providing real-time
financial decisions, which (ideally) will be equivilant to that of a day time trader.

Technical Information:

AI: Python

Front-end: Angular7

Back-end: Flask

Deployment: AWS (Most likely)
